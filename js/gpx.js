xot = new XML.ObjTree;
reader = new FileReader();

getXML = (file, some) => {
	// console.log('file', file, new FileReader(file.files[0]) );

	reader.onload = fileLoaded;
	reader.readAsText(file.files[0]);
};

fileLoaded = event => {
	// console.log('event.target', event.target );
	window.gpx = xot.parseXML(event.target.result);
	console.log('parse', xot.parseXML(event.target.result) );

// 	console.log('obj', xot.parseXML(event.target.result).gpx.trk );
	console.log('gpx', gpx );
};

checkData = text => {
	return /^<\?xml/.test(text.trim())
		&& /<gpx/.test(text.trim())
		&& /<trk>/.test(text.trim())
		&& /<(trkseg|segment)>/.test(text.trim())
}

processTrack = track => {
	// console.log( 'track.value', track.value )
	// console.log( 'checkData(track.value)', checkData(track.value) )

	if (checkData(track.value)) {
		window.gpx = xot.parseXML(track.value).gpx

		// console.log( 'gpx.trk', gpx.trk )

		if (gpx.segment) {
			processPoints(gpx.segment.trkpt)

		} else if (gpx.trk && gpx.trk.trkseg) {
			processPoints(gpx.trk.trkseg.trkpt)
		}
	} else console.warn('Incorrect data! Can not create xml document!');
}

processPoints = (points = []) => {
	// console.log( 'points', points )
	// console.log( 'points[0]', points[0] )

	totalDistance = 0
	lastItemIndex = points.length - 1

	// get distances
	points.forEach((item, index) => {
		prevItem = points[ index - 1 ]

		isLastItem = index == lastItemIndex

		// if (index && !isLastItem) {
		if (index) {
			distanceToPrev = +google.maps.geometry.spherical.computeDistanceBetween(
				new google.maps.LatLng(...[ prevItem['-lat'], prevItem['-lon'] ]),
				new google.maps.LatLng(...[ +item['-lat'], +item['-lon'] ])
			)
			totalDistance += distanceToPrev
			item.distance = distanceToPrev
			// console.log( 'distanceToPrev', distanceToPrev )
		}
		// console.log( 'points', points )
	})

	// console.log( 'totalDistance', totalDistance )
	// console.log( 'points[7]', points[7] )
	// console.log( 'points[ lastItemIndex ]', points[ lastItemIndex ] )

	// fill coefficient
	points.forEach((item, index) => {
		item.coefficient = index && +((item.distance * 100) / totalDistance)
	})

	firstDate = new Date(points[0].time)
	updated = firstDate
	lastDate  = new Date(points[ lastItemIndex ].time)

	// console.log( 'firstDate', firstDate )
	// console.log( 'lastDate', lastDate )

	totalTime = lastDate.getTime() - firstDate.getTime()

	// console.log( 'totalTime', totalTime )

	// calculate date/time
	points.forEach((item, index) => {
		time = +((totalTime * item.coefficient)/100).toFixed(0)
		updated = new Date(updated.getTime() + time);
		item.time
			? item.newTime = updated.toISOString()
			: item.time = updated.toISOString()
	})

	renderSegment(points)
}

renderSegment = pointList => {
	// console.log( 'pointList', pointList )

	output = ''

	pointList.forEach(item => {
		lat = (+item['-lat']).toFixed(7)
		lon = (+item['-lon']).toFixed(7)

		output += `<trkpt lat="${ lat }" lon="${ lon }"><ele>${ item.ele }</ele><time>${ item.time }</time></trkpt>\n`
	})

	window.segmentResult?.remove()
	document.querySelector('.uk-container')?.insertAdjacentHTML('beforeend',
		`<div id=segmentResult>
			<h2 class="uk-heading-small uk-margin-small-top" >Fixed segment</h2>
			<textarea class="uk-textarea uk-height-medium" >${ output }</textarea>
		</div>`)
}







// xot = new XML.ObjTree;
// gpx = xot.parseXML(`<?xml version="1.0" encoding="UTF-8"?>`);

var bar = document.getElementById('js-progressbar');

UIkit.upload('.js-upload', {
	url: '',
	// multiple: true,

	beforeSend: function () {
		console.log('beforeSend', arguments);
	},
	beforeAll: function () {
		console.log('beforeAll', arguments);
	},
	load: function () {
		console.log('load', arguments);
	},
	error: function () {
		console.log('error', arguments);
	},
	complete: function () {
		console.log('complete', arguments);
	},

	loadStart: function (e) {
		console.log('loadStart', arguments);

		bar.removeAttribute('hidden');
		bar.max = e.total;
		bar.value = e.loaded;
	},

	progress: function (e) {
		console.log('progress', arguments);

		bar.max = e.total;
		bar.value = e.loaded;
	},

	loadEnd: function (e) {
		console.log('loadEnd', arguments);

		bar.max = e.total;
		bar.value = e.loaded;
	},

	completeAll: function () {
		console.log('completeAll', arguments);

		setTimeout(function () {
			bar.setAttribute('hidden', 'hidden');
		}, 1000);

		alert('Upload Completed');
	}
});
