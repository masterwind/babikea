// ==UserScript==
// @name         Avto Pro User Utils
// @description  Some user utils
// @namespace    avto-pro-masterwind
// @author       masterwind
// @version      24.8.9
// @match        https://avtopro.ua/*
// @match        https://avto.pro/*
// @updateURL    https://babikea.netlify.app/js/avto.pro.user.js
// @downloadURL  https://babikea.netlify.app/js/avto.pro.user.js
// @supportURL   https://bitbucket.org/masterwind/jira/issues/
// @icon         data:image/svg+xml,%3C%3Fxml version='1.0' encoding='utf-8'%3F%3E%3Csvg version='1.1' id='avtopro_ua_favicon_svg' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' viewBox='0 0 16 16' enable-background='new 0 0 16 16' xml:space='preserve'%3E%3ClinearGradient id='SVGID_1_' gradientUnits='userSpaceOnUse' x1='8' y1='13.7048' x2='8' y2='-3.8287'%3E%3Cstop offset='0' style='stop-color:%231D1D1B'/%3E%3Cstop offset='1' style='stop-color:%23AFAFAF'/%3E%3C/linearGradient%3E%3Cpath fill='url(%23SVGID_1_)' d='M14.001,9.72h1.809C15.931,9.166,16,8.591,16,8c0-4.418-3.582-8-8-8C3.582,0,0,3.582,0,8 c0,4.418,3.582,8,8,8c3.189,0,5.935-1.871,7.22-4.571h-0.628C14.334,10.889,14.132,10.319,14.001,9.72z'/%3E%3ClinearGradient id='SVGID_2_' gradientUnits='userSpaceOnUse' x1='8.9048' y1='5.6068' x2='8.9048' y2='11.7318'%3E%3Cstop offset='0' style='stop-color:%23F2F2F2'/%3E%3Cstop offset='1' style='stop-color:%23AFAFAF'/%3E%3C/linearGradient%3E%3Cpath fill='url(%23SVGID_2_)' d='M11.852,9.72l1.768-5.339H7.615C4.807,4.381,2,5.876,2,8.866c0,2.99,3.887,3.631,5.831,1.495 c0,0.64,0.432,1.068,1.728,1.068h5.661c0.257-0.54,0.459-1.11,0.59-1.709H11.852z M7.831,9.72c-1.08,0-1.728-0.214-1.728-0.854 c0-0.427,0.816-2.596,0.928-2.99h2.069L7.831,9.72z'/%3E%3C/svg%3E
// @grant        none
// @noframes
// ==/UserScript==

/* eslint-disable no-multi-spaces */

class CustomUserFilter {
	constructor() {
		this.setStyles()
		this.setSVGSprite()
		this.setControls()
	}

	setStyles() {
		document.head.insertAdjacentHTML('beforeend', `<style>
		@keyframes apu-spin-clockwise { to { rotate: 360deg; } }
		@keyframes apu-spin-counter-clockwise { to { rotate: -360deg; } }

		.apu-rotating-clockwise svg { animation: apu-spin-clockwise 2s linear infinite; }
		.apu-rotating-counter-clockwise svg { animation: apu-spin-counter-clockwise 2s linear infinite; }

		.apu-icon.icon-tabler { width: 2rem; height: 2rem; color: #555; }
		.apu-button.apu-button { line-height: 0; padding: 0; border: 0; background: none; z-index:1; }
		.apu-filter-controls { display:flex; align-items:center; gap:.5rem; }
		</style>`)
	}

	setSVGSprite() {
		let svgSprites = `<svg xmlns="https://www.w3.org/2000/svg" style="position:fixed;top:0;left:110vw;">
			${ Object.values( this.#SVGSprite() ).join('') }
		</svg>`

		document.body.insertAdjacentHTML( 'beforeend', svgSprites )
	}

	setControls() {
		let partsHeaderElement = document.querySelector('.ap-feed__header')

		if ( partsHeaderElement ) {
			this.controlsWrapperElement = document.createElement('div')
			this.controlsWrapperElement.classList.add('apu-filter-controls')

			let uploadControlElement = document.createElement('button')

			Object.assign(uploadControlElement, {
				id        : 'uploadAllPartsHandlerButton',
				className : 'apu-button',
				onclick   : this.uploadAllPartsHandler.bind(this, uploadControlElement),
				title     : 'Upload all parts',
				innerHTML : '<svg class="apu-icon icon-tabler"><use xlink:href="#refresh_dot"></use></svg>',
			})

			this.controlsWrapperElement.appendChild( uploadControlElement )

			partsHeaderElement.appendChild( this.controlsWrapperElement )
		}
	}

	setFilterControl() {
		let filterControlElement = document.createElement('button')

		Object.assign(filterControlElement, {
			id        : 'filterPartsHandlerButton',
			className : 'apu-button',
			onclick   : this.filterPartsHandler.bind(this, filterControlElement),
			title     : 'Filtering duplicate parts',
			innerHTML : '<svg class="apu-icon icon-tabler"><use xlink:href="#copy_off"></use></svg>',
		})

		this.controlsWrapperElement?.appendChild( filterControlElement )
	}

	uploadAllPartsHandler(button, event) {
		button.classList.add('apu-rotating-counter-clockwise')

		if ( !this.uploadIntervalId ) this.uploadIntervalId = setInterval(this.triggerUploadButton.bind(this, button), 1000)
	}

	triggerUploadButton(button, event) {
		let liveLoadButton = self['js-btn-partslist-primary-showmore']

		// skip if button has not found
		if ( !liveLoadButton ) return

		// check if upload button not disabled
		if ( !liveLoadButton.disabled && !liveLoadButton.hidden ) {
			liveLoadButton.click()
			return
		}

		// update controls, clear interval & sort parts after uploaded all
		if ( liveLoadButton.hidden ) {
			clearInterval( this.uploadIntervalId )

			this.hideUploadControl(button)
			this.setFilterControl()
			this.sortPriceByAscending()
		}
	}

	sortPriceByAscending() {
		let sortControl = document.querySelector('[data-sort-by="price"]')

		sortControl?.click()
	}

	hideUploadControl(button) {
		button.classList.remove('apu-rotating-counter-clockwise')
		button.classList.add('hidden')
	}

	filterPartsHandler() {
		let seenItems = {}

		document.querySelectorAll('#js-partslist-primary tbody tr').forEach( item => {
			let [ expandItem, makerItem, codeItem, imageItem, descriptionItem, deliveryItem, priceItem ] = item.children

			let maker    = makerItem.textContent.trim()
			let code     = codeItem.textContent.trim()
			let delivery = deliveryItem.textContent.trim()
			let price    = priceItem.textContent.trim()
			let usedItem = item.querySelector('[data-sub-title="Б/В"]')

			// remove used items
			if ( usedItem ) {
				item.remove()
				return
			}

			// remember best price item
			if ( !seenItems[ maker + '_' + code ] && delivery.match(/Сьогодні|Завтра|У наявності/) ) {
				seenItems[ maker + '_' + code ] = 1
				return
			}

			item.remove()
		} )
	}

	#SVGSprite() {
		return {
			refresh_dot : '<symbol id="refresh_dot" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="icon icon-tabler icons-tabler-outline icon-tabler-refresh-dot"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M20 11a8.1 8.1 0 0 0 -15.5 -2m-.5 -4v4h4" /><path d="M4 13a8.1 8.1 0 0 0 15.5 2m.5 4v-4h-4" /><path d="M12 12m-1 0a1 1 0 1 0 2 0a1 1 0 1 0 -2 0" /></symbol>',
			copy_off    : '<symbol id="copy_off" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="icon icon-tabler icons-tabler-outline icon-tabler-copy-off"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M19.414 19.415a2 2 0 0 1 -1.414 .585h-8a2 2 0 0 1 -2 -2v-8c0 -.554 .225 -1.055 .589 -1.417m3.411 -.583h6a2 2 0 0 1 2 2v6" /><path d="M16 8v-2a2 2 0 0 0 -2 -2h-6m-3.418 .59c-.36 .36 -.582 .86 -.582 1.41v8a2 2 0 0 0 2 2h2" /><path d="M3 3l18 18" /></symbol>',
		}
	}
}

new CustomUserFilter()
