tpl = {
	// utils
	get : (tmpl, data,
		// prerender template conditions
		condition = /\?\?(.+?)\?\?/.test(tmpl)
			&& (tmpl = tmpl.replace(/\?\?(.+?)=(.+?)\?\?/g, (match, name, template) => data[name] && template || '')),
	) => tmpl.replace(/\{\{(.+?)\}\}/g, (match, group) => data[group] || ''),

	// templates
	content : {
		legend  : '<span><span legend-marker {{marker}} style="color:{{color}};"></span>Бабайка #{{id}}</span>',
		balloon : 'baBikeA #{{mode}}<br>order {{index}}<br><a target=_blank href="https://www.google.com/maps/dir/?api=1&destination={{destination}}">open</a>',
	},
};
babikea = {
	params : [
		null,
		// babikea #1 config
		{
			// baloon : {balloonContent : ``, },  // here a custom balloon content
			marker : {preset:'islands#circleIcon', iconColor:'#008000', },
			coords : [
				[49.384667, 32.194028],
				[49.485528, 31.994331],
				[49.432468, 32.057019],
				[49.426338, 32.010596],
				[49.435552, 32.057169],
				[49.463895, 32.032375],
				[49.454033, 32.041921],
				[49.395936, 32.056805],
				[49.398108, 32.127937],
				[49.506594, 31.945186],
				[49.384889, 32.194500],
				[49.443828, 32.095186],
				[49.408059, 32.090227],
				[49.390745, 32.106233],
				[49.449752, 32.063452],
				[49.306384, 32.010423],
				[49.406130, 32.048836],
				[49.432467, 32.056792],
				[49.470709, 32.023721],
				[49.416316, 31.900961],
				[49.518070, 31.948132],
				[49.470942, 31.922916],
				[49.353486, 32.124167],
				[49.365261, 32.170688],
				[49.471079, 31.978266],
				[49.404097, 31.919474],
				[49.450045, 31.978207],
				[49.435328, 32.100511],
				[49.446625, 32.024795],
				[49.412393, 32.120963],
				[49.352848, 32.062907],
				[49.359994, 31.962276],
				[49.503628, 32.184771],
				[49.339750, 32.067631],
				[49.471521, 32.029259],
				[49.355851, 31.957698],
				[49.379647, 32.220094],
				[49.431938, 31.979388],
				[49.366384, 32.082330],
				[49.493564, 31.945592],
			],
			circle : {
				fill : '#00800029',
				radius : [14000],
			},
		},
		// babikea #2 config
		{
			// baloon : {balloonContent : ``, },  // here a custom balloon content
			marker : {preset:'islands#circleDotIcon', iconColor:'#ed4543', },
			coords : [
				[49.250512, 31.882938],
				[49.621005, 31.697435],
				[49.223720, 32.105794],
				[49.369577, 31.665750],
				[49.233776, 31.984351],
				[49.168773, 32.389953],
				[49.255951, 32.303674],
				[49.517964, 31.915685],
				[49.503883, 32.220423],
				[49.257177, 32.125413],
				[49.179463, 32.365811],
				[49.354110, 31.611816],
			],
			circle : {
				fill : '#ed454329',
				radius : [54000],
			},
		},
		// babikea #3 config
		{
			// baloon : {balloonContent : ``, },  // here a custom balloon content
			marker : {preset:'islands#circleIcon', iconColor:'#0e4779', },
			coords : [
				[48.864534, 32.451178],
				[49.542517, 30.876356],
				[49.982090, 31.385067],
			],
			circle : {
				fill : '#0e477929',
				radius : [104000],
			},
		},
	],
	config : {
		center : [49.42634183, 32.04931051],
	},
	renderMap : () => babikea.map = new ymaps.Map('map', {center:babikea.config.center, zoom:9, }),
	// set map legend
	setLegend : () => window.legend
		&& !legend.insertAdjacentHTML('beforeend', babikea.getLegendItems()),
	getLegendItems : () => babikea.params.map((
			item, id, arr, marker = item && item.marker.preset.replace(/^.+\#([a-zA-Z]+)/, '$1')
		) => item && tpl.get(
			tpl.content.legend,
			{marker, color:item.marker.iconColor, id, }
		)
	).join(''),
	// render markers
	setMarker : (coords, index, mode ) => babikea.map.geoObjects.add(new ymaps.Placemark(
		coords,
		babikea.params[mode].baloon || {balloonContent:tpl.get(tpl.content.balloon, {mode, index:++index, destination:coords.join()})},
		babikea.params[mode].marker || {}
	)),
	processMarkers : () => babikea.params.map((item, id, arr ) => item
		&& item.coords.map((coords, index ) => babikea.setMarker(coords, index, id))),
	// render circles
	setCircle : (item, radius ) => babikea.map.geoObjects.add(new ymaps.Circle(
		[babikea.config.center, radius],  // center & radius
		{},  // baloon & hint
		{  // color params
			fillColor : item.circle.fill,
			strokeColor : item.marker.iconColor,
			strokeWidth : 2,
		}
	)),
	processCircles : () => babikea.params.map(item => item && item.circle
		&& item.circle.radius.map(rad => babikea.setCircle(item, rad)))
	&& babikea.setCircle({circle:{fill:'#0003'}, marker:{iconColor:'#000'}}, 4000),  // set black zone

	init : () => {
		babikea.renderMap();
		babikea.setLegend();
		babikea.processMarkers();
		babikea.processCircles();

		// TODO
		// save coords separately
		// get coords from file and set they markers
	},
};

window.ymaps && ymaps.ready(babikea.init);
