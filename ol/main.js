// render template
String.prototype.render = function(data) {
	!data && (data = {});
	template = this;
	/\?\?(.+?)\?\?/.test(template) && (template = template.replace(/\?\?(.+?)=(.+?)\?\?/g, function(match, name, tpl) { return data[ name ] ? tpl : '' }));
	return template.replace(/\{\{(.+?)\}\}/g, function(match, group) { return data[ group ] || '' });
};


((bmap) => {
	bmap.init();
	// bmap.setRounds();
})(
	window.bmap = {
		params : {
			center : [49.42634183, 32.04931051],
			zoom   : 10,
		},

		init : () => {
			// create map
			// oLmap = new ol.Map({
			// 	target : 'map',
			// 	layers : [
			// 		new ol.layer.Tile({
			// 			source: new ol.source.OSM(),
			// 		}),
			// 	],
			// 	view   : new ol.View({
			// 		center : ol.proj.fromLonLat(olm.params.center.reverse()),
			// 		zoom   : olm.params.zoom,
			// 	}),
			// 	units  : 'm',
			// });

			// init map
			lMap = L.map('map').setView(bmap.params.center, 10);

			// set map layer
			L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
				attribution : 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
				maxZoom     : 20,
				// id          : 'mapbox/dark-v10',
				id          : 'mapbox/navigation-guidance-night-v4',
				tileSize    : 512,
				zoomOffset  : -1,
				accessToken : 'pk.eyJ1IjoibWFzdGVyd2luZCIsImEiOiJjazdoM2FhYTIwMGVxM2dxc2N2ZnV2MDEzIn0.jWMdnfeKwgqJvhGxxMSgPQ'
			}).addTo(lMap);

			let coordsStorageURL = document.URL.match(/^file:/)
				? 'https://babikea.netlify.app/data/coords.json'
				: '/data/coords.json'

			// get coords data
			fetch(coordsStorageURL, { mode:'no-cors' })
				.then(response => { console.log(response); return response.json() })  // DEBUG
				// .then(response => response.json())
				.then(json => {
					bmap.processJsonData(json);
				});

// data params
		// "color":"black",
		// "fillColor":"black",
		// "fillOpacity":0.1,
		// "radius":5e3,
		// "weight":2,


/*
			// set markers
			[
				[49.384667, 32.194028],
				[49.485528, 31.994331],
				[49.432468, 32.057019],
				[49.426338, 32.010596],
				[49.435552, 32.057169],
				[49.463895, 32.032375],
				[49.454033, 32.041921],
				[49.395936, 32.056805],
				[49.398108, 32.127937],
				[49.506594, 31.945186],
				[49.384889, 32.194500],
				[49.443828, 32.095186],
				[49.408059, 32.090227],
				[49.390745, 32.106233],
				[49.449752, 32.063452],
				[49.306384, 32.010423],
				[49.406130, 32.048836],
				[49.432467, 32.056792],
				[49.470709, 32.023721],
				[49.416316, 31.900961],
				[49.518070, 31.948132],
				[49.470942, 31.922916],
				[49.353486, 32.124167],
				[49.365261, 32.170688],
				[49.471079, 31.978266],
				[49.404097, 31.919474],
				[49.450045, 31.978207],
				[49.435328, 32.100511],
				[49.446625, 32.024795],
				[49.412393, 32.120963],
				[49.352848, 32.062907],
				[49.359994, 31.962276],
				[49.503628, 32.184771],
				[49.339750, 32.067631],
				[49.471521, 32.029259],
				[49.355851, 31.957698],
				[49.379647, 32.220094],
				[49.431938, 31.979388],
				[49.366384, 32.082330],
				[49.493564, 31.945592],
			].forEach((coords, index) => L.marker(coords, { opacity:.95, icon:bmap.getCustomIcon(3)}).bindPopup(`Бабайка #1/${index + 1}`).addTo(lMap));

			// set circle zones
			L.circle(bmap.params.center, {
				color : '#ed4543',
				fillColor : '#ed4543',
				fillOpacity : .1,
				radius : 55000,
				weight : 2,
			}).bindPopup('Бабайка #3 - 50км від межі міста').addTo(lMap);
			L.circle(bmap.params.center, {
				color : 'orange',
				fillColor : 'orange',
				fillOpacity : .1,
				radius : 35000,
				weight : 2,
			}).bindPopup('Бабайка #2 - 30км від межі міста').addTo(lMap);
			L.circle(bmap.params.center, {
				color : '#008000',
				fillColor : '#008000',
				fillOpacity : .1,
				radius : 15000,
				weight : 2,
			}).bindPopup('Бабайка #1 - 10км від межі міста').addTo(lMap);
			L.circle(bmap.params.center, {
				color : 'black',
				fillColor : 'black',
				fillOpacity : .1,
				radius : 5000,
				weight : 2,
			}).bindPopup('Мертва зона, бажано в ній не ховати бабайку').addTo(lMap);

*/

		},


		// process data
		processJsonData : data => {
			data.length
				? data.reverse().forEach(entity => {
					// set default params
					entity.weight = 2;
					entity.fillOpacity = .1;
					// entity.fillColor = entity.color;

					// create circle zones
					L.circle(bmap.params.center, entity).bindPopup(entity.title).addTo(lMap);

					// add markers
					entity.items && entity.items.forEach((item, index, array) => {
						let last = !entity.disabled ? array.length == (index + 1) : false;
						L.marker(item, {
							opacity : last ? .95 : .35,
							icon    : bmap.getCustomIcon(entity.id, last),
						}).bindPopup(
							`Бабайка #{{type}}/{{order}}
							??destination=<a href="https://www.google.com/maps/dir/?api=1&destination={{destination}}&travelmode=walking" target=_blank>run</a>??`.render({
								type        : entity.id,
								order       : index + 1,
								destination : last && item.join(),
							})
						).addTo(lMap);
					});
				})
				: console.warn('Something went wrong!');
		},

		// customize marker
		getCustomIcon : (type, last) => L.icon({
			iconUrl  : `./img/marker${type}.svg`,
			iconSize : last ? [30, 30] : [22, 22],
			// iconAnchor: [22, 94],
			// popupAnchor: [-3, -76],
			// shadowUrl: 'my-icon-shadow.png',
			// shadowSize: [68, 95],
			// shadowAnchor: [22, 94]
		}),
	},
);


/*
console.clear();

data = [
	null,
	{  // babikea 1
		marker : {
			preset:0,
			color: '#008000',
		},
		coords : [
				[49.384667, 32.194028],
				[49.485528, 31.994331],
				[49.432468, 32.057019],
// 				[49.426338, 32.010596],
		],
		circle : {
			fill   : '#00800029',
			radius : [500],
			color  : '', },
	},
];


// set dead-zone circle
deadZone = document.createElement('div');
deadZone.classList.add('dead-zone');
oLmap.addOverlay(new ol.Overlay({
	position    : ol.proj.fromLonLat([49.42634183, 32.04931051].reverse()),
	positioning : 'center-center',
	element     : deadZone,
	stopEvent   : false,
	autoPan     : true,
}));



data.map((baBikeA, index ) => {
console.log(baBikeA, index );
	baBikeA && baBikeA.coords.map((coords, i, a, marker = document.createElement('div') ) => {
		marker.classList.add('custom-marker', `b-${index}`, coords.join('-'));
		marker.innerHTML = `<div>`;
		oLmap.addOverlay(new ol.Overlay({
			position    : ol.proj.fromLonLat(coords.reverse()),
			positioning : 'center-center',
			element     : marker,
			stopEvent   : false,
		}))
	});

});

for (var i = 0; i < 10; i++) {
                    var element = document.createElement('div');
                    element.innerHTML = '<img src="https://cdn.mapmarker.io/api/v1/fa/stack?size=50&color=DC4C3F&icon=fa-microchip&hoffset=1" />';
//                     var marker = new ol.Overlay({
//                         position: [i, i],
//                         positioning: 'center-center',
//                         element: element,
//                         stopEvent: false
//                     });
//                     oLmap.addOverlay(marker);
}


*/
